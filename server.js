const express = require("express");

const mongoose = require("mongoose");

const articleRouter = require("./routes/articles");

const app = express();

mongoose.connect("mongodb://localhost/markdown-blog");

app.set("view engine", "ejs");

app.use(express.urlencoded({ extended: false }));

app.get("/", (req, res) => {
    const articles = [
        {
            title: "Test article",
            createdAt: new Date(),
            description: "Sample description"
        },
        {
            title: "Test article2",
            createdAt: new Date(),
            description: "Sample description2"
        }
    ]
    let context = {
        articles: articles
    }
    res.render("articles/index", context);
}).listen(8080);


app.use("/articles", articleRouter);
